﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrigramCodeKata.Infrastructure.Factory;
using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.Test
{
    [TestClass]
    public class TrigramTest
    {
        [TestMethod]
        public void create_trigram()
        {
            var key = new[] {"A","fine" };
            const string value = "cat";

            ITrigram trigram = new TrigramFactory().Instance(key, value);

            Assert.AreEqual("cat", trigram.Value);
        }


        [TestMethod]
        public void create_trigram_collection()
        {
            ITrigramCollection trigramCollection = new TrigramCollectionFactory().Instance();
            Assert.IsNotNull(trigramCollection);
        }


        [TestMethod]
        public void add_trigram_to_collection()
        {
            string[] key = { "A", "fine" };
            const string value = "cat";

            ITrigram trigram = new TrigramFactory().Instance(key, value);
                
            ITrigramCollection trigramCollection = new TrigramCollectionFactory().Instance();
            trigramCollection.Add(trigram);

            IEnumerable<ITrigram> result = trigramCollection.Where(x => x.Key.Equals(key));
            Assert.AreEqual("cat", result.First().Value);
        }



        [TestMethod]
        public void first_trigram_in_collection()
        {
            string[] key = { "A", "fine" };

            ITrigramCollection collection = new TrigramCollectionFactory().Instance();

            collection.Add(new TrigramFactory().Instance(key, "cat"));

            IEnumerable<ITrigram> result = collection.GetByKey(key);

            Assert.AreEqual("cat", result.First().Value);
        }
    }
}
