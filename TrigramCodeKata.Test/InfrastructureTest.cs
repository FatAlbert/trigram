﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrigramCodeKata.Infrastructure.Wiring;

namespace TrigramCodeKata.Test
{
    [TestClass]
    public class InfrastructureTest
    {
        [TestMethod]
        [ExpectedException(typeof(InfrastructureConfigurationException))]
        public void create_text_service_instance()
        {
            Ioc.GetContainer();
        }
        
    }

    [TestClass]
    public class BarMeTest1
    {

        [TestMethod]
        public void foo_with_inheritance()
        {
            IFoo foo = new Foo();
            string str = foo.BarMe();
            Assert.AreEqual(str, "bar");

             foo = new SuperFoo();
             str = foo.BarMe();
             Assert.AreEqual(str, "super bar");
        }

        public interface IFoo
        {
            string BarMe();
        }

        public class Foo : IFoo
        {
            public virtual string BarMe()
            {
                return "bar";
            }
        }

        public class SuperFoo : IFoo
        {
            public string BarMe()
            {
                return "super bar";
            }
        }
    }


    [TestClass]
    public class BarMeTest2
    {

        [TestMethod]
        public void foo_with_interface_as_behavior()
        {
            var foo = new Foo(new DefaultBehavior());
            string str = foo.BarMe();
            Assert.AreEqual(str, "bar");

            foo = new Foo(new SuperBehavior());
            str = foo.BarMe();
            Assert.AreEqual(str, "super bar");
        }

        public interface IBarMeBehavior
        {
            string Execute();
        }
        
        public class DefaultBehavior : IBarMeBehavior
        {
            public string Execute()
            {
                return "bar";
            }
        }
        
        public class SuperBehavior : IBarMeBehavior
        {
            public string Execute()
            {
                return "super bar";
            }
        }

        public class Foo 
        {
            private readonly IBarMeBehavior _barMeBehavior;

            public Foo(IBarMeBehavior barMeBehavior)
            {
                _barMeBehavior = barMeBehavior;
            }

            public string BarMe()
            {
                return _barMeBehavior.Execute();
            }
        }
    }
}
