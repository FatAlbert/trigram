﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrigramCodeKata.Infrastructure.Factory;
using TrigramCodeKata.Facade.Model;
using TrigramCodeKata.Infrastructure.Wiring;

namespace TrigramCodeKata.Test
{
    [TestClass]
    public class TrigramManagerTest 
    {
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            var ioc = new Ioc();
            ioc.SetUp();
        }

        [TestMethod]
        public void get_new_text()
        {
            ITrigramManager trigramManager = new TrigramManagerFactory().Instance("The fine cat lay in the morning sun");

            Assert.AreEqual("The fine cat lay in the morning sun", trigramManager.NewText());
        }

        [TestMethod]
        public void punctuation_in_text()
        {
            ITrigramManager trigramManager = new TrigramManagerFactory().Instance("A fine cat. With a nice hat");

            Assert.AreEqual("A fine cat. With a nice hat", trigramManager.NewText());
        }

        [TestMethod]
        public void skip_characters()
        {
            ITrigramManager trigramManager = new TrigramManagerFactory().Instance("A fine cat. \"With a nice hat\", said the girl");

            Assert.AreEqual("A fine cat. With a nice hat, said the girl", trigramManager.NewText());
        }
    }
}
