﻿using Microsoft.Practices.Unity;
using TrigramCodeKata.Infrastructure.Wiring;

namespace TrigramCodeKata.Infrastructure.Factory
{
    public class Factory<T>
    {
        protected T Instance()
        {
            return Ioc.GetContainer().Resolve<T>();
        }

        protected T Instance(string parameterName, object parameterValue)
        {
            return Ioc.GetContainer().Resolve<T>(new ParameterOverride(parameterName, parameterValue));
        }
    }
}
