﻿using TrigramCodeKata.BusinessLogic.Manager;
using TrigramCodeKata.BusinessLogic.Model;
using TrigramCodeKata.BusinessLogic.Service;
using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.Infrastructure.Wiring
{
    public class TextFromFileIoc : Ioc
    {
        private readonly string _pathToTextFile;

        public TextFromFileIoc(string pathToTextFile)
        {
            _pathToTextFile = pathToTextFile;
        }

        public override void SetUp()
        {
            base.SetUp();
            RegisterInstance(typeof(ITextService), new TextFromFileService(_pathToTextFile));
            RegisterType(typeof(ITrigramManager), typeof(RandomTrigramManager));
            RegisterType(typeof(ITrigramCollection), typeof(RandomTrigramCollection));
            //RegisterType(typeof(ICollectionFromTextBehavior), typeof(VeryStrangeCollectionFromTextBehavior));
        }
    }
}