﻿using System;
using Microsoft.Practices.Unity;
using TrigramCodeKata.BusinessLogic.Manager;
using TrigramCodeKata.BusinessLogic.Model;
using TrigramCodeKata.BusinessLogic.Service;
using TrigramCodeKata.Facade.Model;
using TrigramCodeKata.Infrastructure.Factory;

namespace TrigramCodeKata.Infrastructure.Wiring
{
    public class Ioc
    {
        protected static UnityContainer Container;

        public Ioc()
        {
            Container = new UnityContainer();
        }

        public virtual void SetUp()
        {
            if (!Container.IsRegistered(typeof(ITrigramManager)))
            {
                RegisterType(typeof(ITrigram), typeof(Trigram));
                RegisterType(typeof(ITrigramCollection), typeof(TrigramCollection));
                RegisterType(typeof(ITrigramFactory), typeof(TrigramFactory));
                RegisterType(typeof(ITrigramCollectionFactory), typeof(TrigramCollectionFactory));
                RegisterType(typeof(ITrigramManager), typeof(TrigramManager));
                RegisterType(typeof(ITrigramManagerFactory), typeof(TrigramManagerFactory));
                RegisterType(typeof(ITextService), typeof(PlainTextService));
                RegisterType(typeof(ICollectionFromTextBehavior), typeof(DefaultCollectionFromTextBehavior));
            }
        }

        protected static void RegisterType(Type fromType, Type toType)
        {
            Container.RegisterType(fromType, toType);
        }

        protected static void RegisterInstance(Type fromType, object instance)
        {
            Container.RegisterInstance(fromType, instance);
        }

        public T GetInstance<T>()
        {
            return Container.Resolve<T>();
        }

        public static UnityContainer GetContainer()
        {
            if (Container == null || !Container.IsRegistered(typeof(ITrigramManager)))
            {
                throw new InfrastructureConfigurationException();
            }

            return Container;
        }
    }
}
