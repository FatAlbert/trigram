﻿namespace TrigramCodeKata.Infrastructure.Wiring
{
    public class InfrastructureConfigurationException : System.Exception
    {
        public InfrastructureConfigurationException()
            : base("Ioc container not instantiated")
        {}
    }
}