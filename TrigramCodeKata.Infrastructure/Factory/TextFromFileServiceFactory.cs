﻿using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.Infrastructure.Factory
{
    public class TextFromFileServiceFactory : Factory<ITextService>
    {
        public ITextService Instance(string path)
        {
            return Instance("pathToTextFile", path);
        }
    }
}