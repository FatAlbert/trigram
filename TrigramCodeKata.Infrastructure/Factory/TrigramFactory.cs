﻿using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.Infrastructure.Factory
{
   public class TrigramFactory : Factory<ITrigram>, ITrigramFactory
   {
       public ITrigram Instance(string[] key, string value)
       {
           var trigram = Instance();
           trigram.Key = key;
           trigram.Value = value;
           return trigram;
       }
    }
}
