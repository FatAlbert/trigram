﻿using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.Infrastructure.Factory
{
    public class TrigramCollectionFactory : Factory<ITrigramCollection>, ITrigramCollectionFactory
    {
       public ITrigramCollection Instance()
       {
           return base.Instance();
       }
    }
}
