﻿using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.Infrastructure.Factory
{
    public class TrigramManagerFactory : Factory<ITrigramManager>, ITrigramManagerFactory
    {
        public ITrigramManager Instance(string text= "")
        {
            if (string.IsNullOrEmpty(text))
            {
                return base.Instance();
            }
            return base.Instance("text", text);
        }
    }
}
