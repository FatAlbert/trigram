﻿using System.Collections.Generic;

namespace TrigramCodeKata.Facade.Model
{
    public interface ITextService
    {
        string GetText();
    }
}
