﻿namespace TrigramCodeKata.Facade.Model
{
    public interface ITrigramFactory
    {
        ITrigram Instance(string[] key, string value);
    }
}