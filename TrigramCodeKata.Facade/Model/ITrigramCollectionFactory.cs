﻿namespace TrigramCodeKata.Facade.Model
{
    public interface ITrigramCollectionFactory
    {
        ITrigramCollection Instance();
    }
}