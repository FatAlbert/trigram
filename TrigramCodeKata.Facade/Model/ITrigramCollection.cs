﻿using System.Collections.Generic;

namespace TrigramCodeKata.Facade.Model
{
    public interface ITrigramCollection : IEnumerable<ITrigram>
    {
        void Add(ITrigram trigram);
        ITrigram this[int index] { get; }
        IEnumerable<ITrigram> GetByKey(string[] key);
        string Text { get; }
    }
}
