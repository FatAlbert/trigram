﻿namespace TrigramCodeKata.Facade.Model
{
    public interface ITrigramManager
    {
        string NewText();
    }
}
