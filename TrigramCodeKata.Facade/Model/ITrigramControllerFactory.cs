﻿namespace TrigramCodeKata.Facade.Model
{
    public interface ITrigramManagerFactory
    {
        ITrigramManager Instance(string text = "");
    }
}