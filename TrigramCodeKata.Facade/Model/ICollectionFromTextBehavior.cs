﻿namespace TrigramCodeKata.Facade.Model
{
    public interface ICollectionFromTextBehavior
    {
        ITrigramCollection GetTrigramCollectionFromText(string text);
    }
}