﻿using System.Collections.Generic;

namespace TrigramCodeKata.Facade.Model
{
    public interface ITrigram
    {
        string Value { get; set; }
        string[] Key { get; set; }
        string KeyAsString { get; }
        
    }
}
