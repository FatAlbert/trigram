﻿using System;
using System.Linq;
using TrigramCodeKata.Facade.Model;
using TrigramCodeKata.Infrastructure.Wiring;

namespace TrigramCodeKata.Run
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathToTextFile = args.First();

            var ioc = new TextFromFileIoc(pathToTextFile);

            ioc.SetUp();

            var trigramManagerFactory = ioc.GetInstance<ITrigramManagerFactory>();

            ITrigramManager trigramManager = trigramManagerFactory.Instance();

            var newText = trigramManager.NewText();

            Console.Write(newText);

            Console.ReadKey();
        }
    }
}
