﻿
using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.BusinessLogic.Service
{
    public class PlainTextService : ITextService
    {
        private readonly string _text;

        public PlainTextService(string text)
        {
            _text = text;
        }

        public string GetText()
        {
            return _text;
        }
    }
}
