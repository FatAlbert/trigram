using System.IO;
using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.BusinessLogic.Service
{
    public class TextFromFileService : ITextService
    {
        private readonly string _pathToTextFile;

        public TextFromFileService(string pathToTextFile)
        {
            _pathToTextFile = pathToTextFile;
        }

        public string GetText()
        {
            return File.ReadAllText(_pathToTextFile);
        }
    }
}