﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.BusinessLogic.Model
{
    public class TrigramCollection : ITrigramCollection
    {
        protected readonly List<ITrigram> Trigrams;
        protected int MaxWordsInNewText { get; set; }

        public TrigramCollection()
        {
            Trigrams = new List<ITrigram>();
            MaxWordsInNewText = 1000;
        }

        public ITrigram this[int index]
        {
            get { return Trigrams[index]; }
        }

        public IEnumerable<ITrigram> GetByKey(string[] key)
        {
            return Trigrams.Where(trigram => trigram.Key[0] == key[0] && trigram.Key[1] == key[1]).ToList();
        }

        public void Add(ITrigram trigram)
        {
            Trigrams.Add(trigram);
        }

        public IEnumerator<ITrigram> GetEnumerator()
        {
           return Trigrams.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public string Text {
            get
            {
                var trigram = GetStartTrigram();

                if (trigram == null)
                {
                    return string.Empty;
                }

                return trigram.KeyAsString + " " + GetText(trigram, 0);
                
            }
        }

        protected virtual ITrigram GetStartTrigram()
        {
            return Trigrams.FirstOrDefault();
        }

        protected string GetText(ITrigram trigram, int currentIndex)
        {
            var newTrigrams = GetByKey(new[] { trigram.Key[1], trigram.Value });
            if (!newTrigrams.Any() || currentIndex++ >= MaxWordsInNewText)
            {
                return trigram.Value;
            }
            return trigram.Value + " " + GetText(TrigramSelector(newTrigrams), currentIndex++);
        }

        protected virtual ITrigram TrigramSelector(IEnumerable<ITrigram> newTrigrams)
        {
            return newTrigrams.First();
        }
    }
}
