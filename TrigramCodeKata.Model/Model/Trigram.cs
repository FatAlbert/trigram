﻿using System.Collections.Generic;
using System.Linq;
using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.BusinessLogic.Model
{
    public class Trigram : ITrigram
    {
        public string[] Key { get; set; }

        public string KeyAsString
        {
            get { return Key[0] + " " + Key[1]; }
        }

        public string Value { get; set; }
        public override string ToString()
        {
            return string.Format("{1}{0}{2}{0}{3}", " ", Key.ElementAt(0), Key.ElementAt(1), Value);
        }
    }
}
