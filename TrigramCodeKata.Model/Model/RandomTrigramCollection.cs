﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.BusinessLogic.Model
{
    public class RandomTrigramCollection : TrigramCollection
    {
        protected override ITrigram GetStartTrigram()
        {
            var random = new Random();
            int randomNumber = random.Next(Trigrams.Count());
            return Trigrams.ElementAt(randomNumber);
        }

        protected override ITrigram TrigramSelector(IEnumerable<ITrigram> newTrigrams)
        {
            var random = new Random();
            int randomNumber = random.Next(newTrigrams.Count());
            return newTrigrams.ElementAt(randomNumber);
        }
    }
}
