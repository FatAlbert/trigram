﻿using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.BusinessLogic.Manager
{
    public class TrigramManager : ITrigramManager
    {
        private readonly ICollectionFromTextBehavior _collectionFromTextBehavior;
        public ITrigramCollection TrigramCollection { get; private set; }

        protected string[] CharactersToSkip = { "\"", "-" };
        
        public TrigramManager(ITextService textService, 
            ITrigramCollectionFactory trigramCollectionFactory, 
            ICollectionFromTextBehavior collectionFromTextBehavior)
        {
            TrigramCollection = trigramCollectionFactory.Instance();
            _collectionFromTextBehavior = collectionFromTextBehavior;
            var text = textService.GetText();
            TrigramCollection = Init(text);
        }

        public string NewText()
        {
            return TrigramCollection.Text;
        }

        private ITrigramCollection Init(string text)
        {
            foreach (var character in CharactersToSkip)
            {
                text = text.Replace(character, "");
            }

            return _collectionFromTextBehavior.GetTrigramCollectionFromText(text);
        }
    }
}
