using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.BusinessLogic.Manager
{
    public class RandomTrigramManager : TrigramManager
    {
        public RandomTrigramManager(ITextService textService,
            ITrigramCollectionFactory trigramCollectionFactory, 
            ICollectionFromTextBehavior collectionFromTextBehavior)
            : base(textService,
                trigramCollectionFactory,
            collectionFromTextBehavior)
        {
        }
    }
}