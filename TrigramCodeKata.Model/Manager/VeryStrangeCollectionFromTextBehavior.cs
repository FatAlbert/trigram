﻿using System;
using TrigramCodeKata.Facade.Model;

namespace TrigramCodeKata.BusinessLogic.Manager
{
    public class VeryStrangeCollectionFromTextBehavior : ICollectionFromTextBehavior
    {
        private readonly ITrigramCollectionFactory _trigramCollectionFactory;
        private readonly ITrigramFactory _trigramFactory;

        public VeryStrangeCollectionFromTextBehavior(ITrigramCollectionFactory trigramCollectionFactory,
            ITrigramFactory trigramFactory)
        {
            _trigramCollectionFactory = trigramCollectionFactory;
            _trigramFactory = trigramFactory;
        }

        public ITrigramCollection GetTrigramCollectionFromText(string text)
        {
            var trigramCollection = _trigramCollectionFactory.Instance();

            string[] array = text.Split(new[] { "e" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i + 2 < array.Length; i++)
            {
                string key1 = array[i];
                string key2 = array[i + 1];
                string value = array[i + 2];
                string[] key = { key1, key2 };
                ITrigram trigram = _trigramFactory.Instance(key, value);
                trigramCollection.Add(trigram);
            }
            return trigramCollection;
        }
    }
}
